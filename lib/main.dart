import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(

        appBar: AppBar(
          leading: Icon(Icons.account_balance
          ),
          title: Text('ZPAY App'),

          actions: [IconButton(onPressed: (){}, icon: Icon(Icons.icecream))],
        ),
        body: Center(
          child: Text('Mantita Rabiabdee',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 35,
          ),),

        ),
      ),
    );
  }
}
